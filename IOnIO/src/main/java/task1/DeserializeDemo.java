package task1;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeDemo {
    public static void main(String[] args) {
        Employee employee1 = null;
        try {
            FileInputStream fis = new FileInputStream("D:\\IdeaProjects\\IOnIO\\src\\main\\java\\task1\\employee.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);
            employee1 = (Employee) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("Deserialize Employee...");
        System.out.println("Name " + employee1.name);
        System.out.println("Address " + employee1.address);
        System.out.println("SSN " + employee1.SSN);
        System.out.println("Number " + employee1.number);
    }
}
