package task2;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FileOutputStreamExample {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        try {
            OutputStream os = new FileOutputStream("D:\\IdeaProjects\\IOnIO\\someTextMore.pdf");
            os.write(125);
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        long timeSpent = System.currentTimeMillis() - startTime;
        System.out.println("програма виконувалася " + timeSpent + " мілісекунд");
    }
}
