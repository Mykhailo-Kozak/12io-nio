package task2;

import java.io.*;

public class FileInputStreamExample {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        try {
            //InputStream inputstream = new FileInputStream("D:\\IdeaProjects\\IOnIO\\someTextMore.pdf");
            InputStream inputstream = new BufferedInputStream(new FileInputStream(
                    "D:\\\\IdeaProjects\\\\IOnIO\\\\someTextMore.pdf"), 64*1024);

            int data = inputstream.read();

            while (data != -1) {
                //doSomethingWithData(data);
                data = inputstream.read();
            }
            inputstream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        long timeSpent = System.currentTimeMillis() - startTime;
        System.out.println("програма виконувалася " + timeSpent + " мілісекунд");
    }
}
