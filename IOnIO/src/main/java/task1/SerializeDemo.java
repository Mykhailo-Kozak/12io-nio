package task1;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializeDemo {
    public static void main(String[] args) {
        Employee employee = new Employee();
        employee.name = "Orest Nalivayko";
        employee.address = "Melnyka street, Lviv";
        employee.SSN = 123456789;
        employee.number = 687480569;

        try {
            FileOutputStream fos = new FileOutputStream("D:\\IdeaProjects\\IOnIO\\src\\main\\java\\task1\\employee.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(employee);
            oos.close();
            fos.close();
            System.out.println("Serialize data is save in D:\\IdeaProjects\\IOnIO\\src\\main\\java\\task1\\employee.ser");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
