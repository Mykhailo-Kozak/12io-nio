package task2;

import java.io.*;

public class DataInputStreamExample {
    public static void main(String[] args) {

        double price;
        int quantity;
        String item;

        try (
                InputStream is = new FileInputStream("data.txt");
                BufferedInputStream bis = new BufferedInputStream(is);
                DataInputStream in = new DataInputStream(bis)
        ) {
            while (in.available() > 0) {
                quantity = in.read();
                item = in.readUTF();
                quantity = in.readInt();
                price = in.readDouble();
                System.out.format("%-20s %-10d $%.2f%n", item, quantity, price);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
